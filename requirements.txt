flake8==3.7.7
pytest==4.5.0
pytest-cov==2.7.1
pytest-datafiles==2.0
wheel>= 0.34
setuptools>=41.2
