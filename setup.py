from distutils.core import setup
import setuptools

setup(
    name='MyHeroLib',
    version='0.1dev',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    author="John Doe",
    author_email="jdoe@company.com",
    description="A small example package",
    long_description=open('README.txt').read(),
    package_dir={"": "main"},
    packages=setuptools.find_namespace_packages(where="main"),
)
